<?php

/**
 * @file
 * Callbacks for page displays.
 */

function mysite_legacy_structure() {
  $output = array();

  // Start with the roots...
  $query = db_select('node', 'n')
    ->fields('n', array('nid','type', 'title'))
    ->condition('n.status', 1)
    ->condition('n.type', 'subject', '=')
    ;

  $query->leftjoin('field_data_field_subject', 's', "n.nid = s.entity_id AND n.vid = s.revision_id AND s.entity_type = 'node'");
  // $query->addField('s', 'field_subject_target_id', 'subject');
  $query->isNull('s.field_subject_target_id');

  $query->leftjoin('field_data_field_weight', 'w', "n.nid = w.entity_id AND n.vid = w.revision_id AND w.entity_type = 'node'");
  $query->orderBy('w.field_weight_value');
  $query->orderBy('n.title');
  $result = $query->execute()->fetchAll();

  $rows = _mysite_structure_format_rows($result);

  $output[] = array(
    '#theme' => 'item_list',
    '#items' => $rows,
  );

  return $output;
}

/**
 * Format legacy structure output rows.
 */
function _mysite_structure_format_rows($result) {
  $rows = array();
  foreach($result as $item) {
    $link = l($item->title, 'node/' . $item->nid);
    if ($item->type == 'handout') {
      $title = t('Handout: !link', array('!link' => $link));
    }
    else {
      $title = $link;
    }
    $row = array('data' => $title);
    if ($children = _mysite_structure_find_children($item->nid)) {
      $row['children'] = $children;
    }
    $rows[] = $row;
  }
  return $rows;
}

/**
 * Find child nodes.
 */
function _mysite_structure_find_children($nid) {

  $query = db_select('node', 'n')
    ->fields('n', array('nid','type', 'title'))
    ->condition('n.status', 1)
    ->condition('n.nid', $nid, '!=')
    ->condition('n.type', array('subject', 'handout'), 'IN')
    ;
  $query->leftjoin('field_data_field_subject', 's', "n.nid = s.entity_id AND n.vid = s.revision_id AND s.entity_type = 'node'");
  $query->addField('s', 'field_subject_target_id', 'subject');
  $query->condition('s.field_subject_target_id', $nid, '=');

  $query->leftjoin('field_data_field_weight', 'w', "n.nid = w.entity_id AND n.vid = w.revision_id AND w.entity_type = 'node'");
  $query->orderBy('w.field_weight_value');
  $query->orderBy('n.title');

  $result = $query->execute()->fetchAll();
  return _mysite_structure_format_rows($result);
}
